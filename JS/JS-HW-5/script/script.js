var btn = document.querySelectorAll(".btn");
var text = document.querySelectorAll(".text");
var mainText = document.querySelector(".main_text");

for (let i = 0; i < btn.length; i++) {
  btn[i].addEventListener("click", chooseCar);

  function chooseCar() {
    for (var j = 0; j < text.length; j++) {
      text[j].classList.remove("show");
    }
    text[i].classList.add("show");
    mainText.classList.add("hide");
  }
}
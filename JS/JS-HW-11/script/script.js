const grid = document.querySelector(".grid");
const headerDate = document.querySelector(".header_date");
const currency1 = document.querySelector(".currency1");
const currency2 = document.querySelector(".currency2");
const currencyOptions1 = document.querySelector(".currency1_option");
const currencyOptions2 = document.querySelector(".currency2_option");
const dropDown1 = document.querySelector(".wrapper__dropdown1");
const dropDown2 = document.querySelector(".wrapper__dropdown2");
const input = document.querySelector(".input");
const output = document.querySelector(".output");
const toggle = document.querySelector(".wrapper_button");
const wrapperCorrency1 = document.querySelector(".wrapper_corrency1");
const wrapperCorrency2 = document.querySelector(".wrapper_corrency2");

let chosenCurrency1;
let chosenCurrency2;
let CurrencyArray = []

let data;

fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
  .then((response) => response.json())
  .then((json) => (data = json));

setTimeout(function () {
  // get array for currency list
  data.map((el) => {
    if (["RUB", "XAU", "XAG", "XPT", "XPD", "XDR"].includes(el.cc)) {
      return;
    }
    CurrencyArray.push(el.cc + " - " + el.txt);
  });

  headerDate.innerHTML = data[0].exchangedate;
  // Get all currecy list to grid
  data.forEach((element) => {
    if (["RUB", "XAU", "XAG", "XPT", "XPD", "XDR"].includes(element.cc)) {
      return;
    }
    grid.insertAdjacentHTML(
      "beforeend",
      `<div class="grid_item">
          <div class="grid_cc">${element.cc}</div>
          <div class="grid_txt">${element.txt}</div>
          <div class="grid_rate">${element.rate}</div>
        </div>`
    );
  });

  dropDown1.addEventListener("click", getCurrencyList1);
  dropDown2.addEventListener("click", getCurrencyList2);
  currency1.addEventListener("input", getList1);
  currency2.addEventListener("input", getList2);
  

  // USer input

// CURRENCY 1
  function getOptions(word, CurrencyArray) {
    return CurrencyArray.filter((c) => {
      const regex = new RegExp(word, "gi");
      return c.match(regex);
    });
  }

  function getList1() {
    const options = getOptions(this.value, CurrencyArray);
    const filterResult = options
      .map((CurrencyArray) => {
        const regex = new RegExp(this.value, "g");
        const currency = CurrencyArray.replace(
          regex,
          `<span>${this.value}</span>`
        );
        return `<li><span>${currency}</span></li>`;
      })
      .slice(0, 10)
      .join("");

    currencyOptions1.innerHTML = this.value ? filterResult : null;

    const optionsList = document.querySelectorAll(".currency1_option li ");
    optionsList.forEach((option) => {
      option.addEventListener("click", () => {
        const selectedValue = option.querySelector("span").textContent;
        currency1.value = selectedValue;
        data.forEach((el) => {
          if (el.cc == selectedValue.slice(0, 3)) {
            chosenCurrency1 = el.rate;
          }
        });
        getResult();
        // clean currency list
        while (currencyOptions1.firstChild) {
          currencyOptions1.removeChild(currencyOptions1.firstChild);
        }
      });
    });
  }
  // CURRENCY 2
  function getList2() {
    const options = getOptions(this.value, CurrencyArray);
    const filterResult = options
      .map((CurrencyArray) => {
        const regex = new RegExp(this.value, "g");
        const currency = CurrencyArray.replace(
          regex,
          `<span>${this.value}</span>`
        );
        return `<li><span>${currency}</span></li>`;
      })
      .slice(0, 10)
      .join("");

    currencyOptions2.innerHTML = this.value ? filterResult : null;

    const optionsList = document.querySelectorAll(".currency2_option li ");
    optionsList.forEach((option) => {
      option.addEventListener("click", () => {
        const selectedValue = option.querySelector("span").textContent;
        currency2.value = selectedValue;
        data.forEach((el) => {
          if (el.cc == selectedValue.slice(0, 3)) {
            chosenCurrency2 = el.rate;
          }
        });
        getResult();
        // clean currency list
        while (currencyOptions2.firstChild) {
          currencyOptions2.removeChild(currencyOptions2.firstChild);
        }
      });
    });
  }


  // USE DROP DOWN
  // create currency list for choisen currency 1
  function getCurrencyList1() {
    CurrencyArray.forEach((el) => {
      let li = document.createElement("li");
      li.innerHTML = el;
      currencyOptions1.insertAdjacentElement("afterbegin", li);
    });
    // insert value to input currency when user choise corrency and clean currency list when user clicks on needed currency
    const currencyList1 = document.querySelectorAll(".currency1_option li");

    currencyList1.forEach((el) => {
      el.addEventListener("click", () => {
        const selectedValue = el.textContent;
        currency1.value = selectedValue;
        data.forEach((el) => {
          if (el.cc == selectedValue.slice(0, 3)) {
            chosenCurrency1 = el.rate;
          }
        });
        getResult()
        // clean currency list
        while (currencyOptions1.firstChild) {
          currencyOptions1.removeChild(currencyOptions1.firstChild);
        }
      });
    });
  }

  // create currency list for choisen currency 2
  function getCurrencyList2() {
    CurrencyArray.forEach((el) => {
      let li = document.createElement("li");
      li.innerHTML = el;
      currencyOptions2.insertAdjacentElement("afterbegin", li);
    });
    // insert value to input currency and clean currency list
    const currencyList2 = document.querySelectorAll(".currency2_option li");
    currencyList2.forEach((el) => {
      el.addEventListener("click", () => {
        const selectedValue = el.textContent;
        currency2.value = selectedValue;
        data.forEach((el) => {
          if (el.cc == selectedValue.slice(0, 3)) {
            chosenCurrency2 = el.rate;
          }
        });
        getResult();
        // clean currency list
        while (currencyOptions2.firstChild) {
          currencyOptions2.removeChild(currencyOptions2.firstChild);
        }
      });
    });
  }
}, 500);

// toogel currency with value and get result for currency exhange

    toggle.addEventListener("click", () => {
    wrapperCorrency1.classList.toggle("order3");
    wrapperCorrency2.classList.toggle("order1");
    getResult();
    });

// Get result for currency exhange
     input.addEventListener("input", () => {
       getResult();
     });

   
    function getResult() {
    if (
        chosenCurrency1 &&
        chosenCurrency2 &&
        wrapperCorrency1.classList.contains("order3")
    ) {  
        let result = (chosenCurrency2 / chosenCurrency1) * input.value;
        output.innerHTML = result.toFixed(2);
        console.log(result);
    } else if (  
        chosenCurrency1 &&
        chosenCurrency2 &&
        !wrapperCorrency1.classList.contains("order3")
    ) {  
        let result = (chosenCurrency1 / chosenCurrency2) * input.value;
        output.innerHTML = result.toFixed(2);
        console.log(result);
    }    
    }



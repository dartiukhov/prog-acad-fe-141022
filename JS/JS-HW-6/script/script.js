var number = document.querySelectorAll(".number");
var inp = document.querySelector(".calculator__inp");
var sign = document.querySelectorAll(".sign");
var result = document.querySelector(".result");
var clear = document.querySelector(".clear");
var toggleSing = document.querySelector(".toggleSign");
var percent = document.querySelector(".percent");

for (i = 0; i < number.length; i++) {
  number[i].addEventListener("click", insertValue);
}

for (i = 0; i < sign.length; i++) {
  sign[i].addEventListener("click", insertValue);
}

// Приймаємо дані від користувача при натисканні на кнопки та перевіряємо корректність
function insertValue() {
  inp.value += this.innerText;

  // Зменшення розміру шрифта в залежності від кількості символів та обмеження на введення символів
  fontSizeChange()

  // Вимикаємо можливість внесенння двух симовлів "0" на початку введення
  if (inp.value.length == 2 && inp.value[0] === "0" && inp.value[1] === "0") {
    inp.value = "0";
  }

  // Вимикання можливості внесення першими символів '+', '*', '/'
  if (
    inp.value[0] === "+" ||
    inp.value[0] === "*" ||
    inp.value[0] === "/" ||
    inp.value[0] === "%"
  ) {
    inp.value = "";
  }

  // Вимикання можливості введення двох знаків підряд
  if (inp.value.length > 1) {
    for (var i = 0; i < inp.value.length; i++) {
      if (
        (inp.value[i] === "/" ||
          inp.value[i] === "*" ||
          inp.value[i] === "." ||
          inp.value[i] === "-" ||
          inp.value[i] === "+" ||
          inp.value[i] === "%") &&
        (inp.value[i + 1] === "/" ||
          inp.value[i + 1] === "*" ||
          inp.value[i + 1] === "." ||
          inp.value[i + 1] === "-" ||
          inp.value[i + 1] === "+" ||
          inp.value[i + 1] === "%")
      ) {
        inp.value = inp.value.substring(0, i) + inp.value[i + 1];
      }
    }
  }

  // Виведення символу "0" у разі введення першого символа "."

  if (inp.value.length === 1 && inp.value[0] === ".") {
    inp.value = "0" + inp.value;
  }

  // Заборона введення більше одного символу '.' у першому числі
  if (inp.value.length > 2) {
    var count = 0;
    for (i = 0; i < inp.value.length; i++) {
      if (inp.value[i] === ".") {
        count += 1;
        if (
          inp.value.includes("+") ||
          inp.value.includes("-") ||
          inp.value.includes("/") ||
          inp.value.includes("*") ||
          inp.value.includes("%")
        ) {
          break;
        }
        if (count == 2) {
          inp.value = inp.value.slice(0, inp.value.length - 1);
        }
      }
    }
  }
}

// Введення символу залишок від ділення
percent.addEventListener("click", insertValue);

// Виведення результату на екран та зменшення шрифта та символів у результаті для корректного відображення
result.addEventListener("click", getResult);
function getResult() {
    inp.value = eval(inp.value);
    fontSizeChange()
}

// Натискання клавиши "Enter" на клавіатурі доя отримання результату
document.body.addEventListener("keydown", function (e) {
  if (e.keyCode == 13) {
    getResult();
  }
});

// Функція очистити (A/C)
clear.addEventListener("click", clearInput);
function clearInput() {
  inp.value = "";
  inp.style.fontSize = "40px";
}

// Натискання клавіши 'delete' для очистки
document.body.addEventListener("keydown", function (e) {
  if (e.keyCode == 46) {
    clearInput();
  }
});

// Натискання клавіши 'backspace' для видалення останнього введеного символу
document.body.addEventListener("keydown", function (e) {
  if (e.keyCode == 8) {
    inp.value = inp.value.slice(0, inp.value.length - 1);
  }
});

// Функція заміни знаку (+/-)

toggleSing.addEventListener("click", toggleNumberSing);

function toggleNumberSing() {
  var tempInpVAlue = inp.value;
  var numMinus;
  // Відключення можливості заміни знаку першого числа у разі введення після нього арифметичного знаку
  if (
    tempInpVAlue.includes("*", 1) ||
    tempInpVAlue.includes("-", 1) ||
    tempInpVAlue.includes("+", 1) ||
    tempInpVAlue.includes("/", 1) ||
    tempInpVAlue.includes("%", 1)
  ) {
    return;
  }
  // Заміна знаку у разі невиконання першої умови
  else if (tempInpVAlue.indexOf("-") !== -1) {
    numMinus = Number(inp.value) * -1;
    inp.value = numMinus;
  } else {
    numMinus = Number(inp.value) * -1;
    inp.value = numMinus;
  }
}

// Зменьшення розміру шрифта
function fontSizeChange() {
    if (inp.value.length < 9) {
    inp.style.fontSize = "40px";
    }
    if (inp.value.length > 9) {
    inp.style.fontSize = "32px";
    }
    if (inp.value.length > 11) {
    inp.style.fontSize = "28px";
    }
    if (inp.value.length > 13) {
    inp.style.fontSize = "24px";
    }
    if (inp.value.length > 15) {
    inp.style.fontSize = "22px";
    }
    if (inp.value.length > 15) {
    inp.value = inp.value.substring(0, 15);
    }
}



const key = document.querySelector(".key"),
  code = document.querySelector(".code"),
  which = document.querySelector(".which"),
  historyBlock = document.querySelector(".historyBlock"),
  headerCode = document.querySelector(".header__code"),
  headerWhich = document.querySelector(".header__which");
let array = [];

// Робота з історією як на референсі та додавання у відповідні комірки значень
window.addEventListener("keyup", (event) => {
  array.forEach((el) => {
    // обмеження на повторне натискання та виведенян до історії потрібних значень
    if (event.key.toUpperCase() === el) {
      const pressedKey = array.indexOf(el);
      historyBlock.removeChild(historyBlock.childNodes[pressedKey]);
      const spliceArray = array.splice(pressedKey, 1);
    }
  });
  //  додаванян в комірки потрібні значення
  key.innerHTML = event.key;
  code.innerHTML = event.code;
  which.innerHTML = event.which;

  addLetterHistory();

  headerCode.innerHTML = event.which;
  headerWhich.innerHTML = event.which;
});

function addLetterHistory() {
  // Додавання історії настискання елементів
  let current = event.key.toUpperCase();
  array.unshift(current);
  let li = document.createElement("li");
  li.innerHTML = array[0];
  historyBlock.insertAdjacentElement("afterbegin", li);

  if (array.length > 4) {
    array = array.slice(0, 4);
    historyBlock.lastElementChild.remove();
  }
  // Вииведення необхідних значень при натисканні на комірки в історії
  let clickedElement = event;
  li.addEventListener("click", () => {
    key.innerHTML = clickedElement.key;
    code.innerHTML = clickedElement.code;
    which.innerHTML = clickedElement.which;
    headerCode.innerHTML = clickedElement.which;
    headerWhich.innerHTML = clickedElement.which;
  });
}

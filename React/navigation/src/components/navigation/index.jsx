import { request } from '../../data/data'
import { useState } from 'react';
import './navigation.css'

function Navigation() {
    let [storeData, setStoreData] = useState();
    function showStore() {
        const url = 'https://fakestoreapi.com/products'
        request(url).then((data) => {
            setStoreData(data);
        })
    }

    

    return (
        <>
            <nav>
                <ul className='navigation'>
                    <li onClick={() => {
                        showStore()
                    }}>Інтернет магазін</li>
                    <li>Список справ</li>
                    <li>Список планет</li>
                </ul>
            </nav>
            <section>
                {
                    Array.isArray(storeData) ? storeData.map((el) => {
                        return <div>{el.title}</div>
                    }): 'Дані не отримані'
                }
            </section>
        </>
    )
}

export default Navigation
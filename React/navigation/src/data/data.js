// https://fakestoreapi.com/products

const request = async (url) => {
    const req = await fetch(url);
    const data = await req.json();
    return data;
}

export { request };
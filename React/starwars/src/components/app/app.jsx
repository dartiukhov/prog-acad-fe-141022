import React, { useState, useEffect } from 'react';
import Title from '../title/title'
import Cards from '../cards/cards'
import Loader from '../loader/loader'
import './app.css'




function App() {
  const [peopleList, setPeople] = useState(true);
  const [loader, setLoader] = useState(true)
  useEffect(() => {
    async function peopleList() {
      try {
        const response = await fetch('https://swapi.dev/api/people/');
        const data = await response.json();
        setPeople(data.results); 
        setLoader(false)
      } catch (error) {
        console.error(error);
      }
    }
    peopleList();
  }, []);
  if (loader) {
    return <Loader />
  }

  return (
    <>
      <div className='content'>
          <Title></Title>
          <Cards data={peopleList}></Cards>
          
      </div>
      </>
      
    )
}

export default App;
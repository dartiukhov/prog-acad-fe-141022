import React from "react";

export default function Loader() {
    return (
        <div className="modal">
            <div className="loader"></div>
        </div>
    )
}
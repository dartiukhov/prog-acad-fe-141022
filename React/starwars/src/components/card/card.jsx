import React from "react";
import './card.css'


function Card({ card }) {
    const url = '/image/heroes/' + card.name.split(' ').join('') + '.png';
    return (
        <div className="card">

            <div className="card__name">{card.name}</div>
            <div className="card__image">
                <img src={url} alt={card.name}/>
            </div>
            <div className="card__abilities">
                <div className="card__left-side">
                    <div>Height:<b>{card.height}</b></div>
                    <div>Mass:<b>{card.mass}</b></div>
                    <div>Hair color:<b>{card.hair_color}</b></div>
                </div>
                <div className="card__right-side">
                    <div>Skin color:<b>{card.skin_color}</b></div>
                    <div>Eye color:<b>{card.eye_color}</b></div>
                    <div>Birth year:<b>{card.birth_year}</b></div>
                </div>
            </div>
            <a href={card.url} target="_blank" className="card__link">About Hero</a>
        </div>    
    )

}

export default Card;

import React from "react";
import Card from '../card/card'
import './cards.css'


function Cards({ data }) {
    return (
        <div className="cards">
            {
                Array.isArray(data) ? data.map((el) => {
                    return <Card card={el}></Card>
                }) : false
            }
        </div>
    )

}

export default Cards


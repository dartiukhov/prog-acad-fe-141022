import React from "react";
import ReactDOM from "react-dom";
import data from "./data";
import "./index.css";


function App() {
    return (
      <div>
        <h1>Курс валют до гривні (НБУ)</h1>
        <table>
          <tr>
            <th>Код валюти</th>
            <th>Назва валюти</th>
            <th>Курс</th>
          </tr>
          {data.map((el) => {
            if ([643, 933, 959, 960, 961, 962, 964].includes(el.r030)) {
              return;
            }
            return (
              <tr>
                <td>{el.cc}</td>
                <td>{el.txt}</td>
                <td>{el.rate.toFixed(2)}</td>
              </tr>
            );
          })}
        </table>
      </div>
    );
}

ReactDOM.render(<App/>, document.getElementById('body'));